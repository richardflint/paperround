Introduction
====================
The program is split into two main packages code for town planning and the paper round.

There are two main entry points for the program and StreetPlanner.java and PaperRoundPlanner.java, this will enable other services to be wrapped around the program such as a web service or GUI.
I have provided a command line application as an example. 

To build the application run ```mvn clean package```

To run the command line application from the projects root run ```java -jar ./target/java -jar paperround-1.0-SNAPSHOT.one-jar.jar```

To run the tests ```mvn clean test```

Assumptions
---------------------

* House numbering starts at number one on the north side of the street


Prerequisites for Dev
---------------------
* Java 1.8
* Maven 2.2.1+

### Running in IDE
If you using the Intellij then you will need to install the Lombok plugin to avoid compilations errors.