package com.valtech.planning.parser;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class FileParserImplTest {
    private FileParser parser;

    @Before
    public void setup(){
        this.parser = new FileParserImpl();
    }

    @Test
    public void shouldReturnListHouseNumbers() throws IOException {
        File streetFile = new File(getClass().getResource("/street_files/two_house_street.txt").getFile());

        List<String> houseNumbers = parser.parse(streetFile);

        assertThat(houseNumbers.size(), is(2));
        assertThat(houseNumbers, hasItems("1", "2"));
    }
}