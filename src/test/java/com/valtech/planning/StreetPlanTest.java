package com.valtech.planning;

import com.google.common.collect.Lists;
import com.valtech.street.House;
import com.valtech.street.StreetPlan;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

import static com.valtech.street.Side.NORTH;
import static com.valtech.street.Side.SOUTH;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class StreetPlanTest {

    private StreetPlan streetPlan;

    @Before
    public void setup(){
        this.streetPlan = new StreetPlan();
    }

    @Test
    public void shouldAddHouseToStreetPlan(){
        assertThat(streetPlan.getAllHouses(), equalTo(Collections.emptyList()));

        House house = new House(1, NORTH);
        streetPlan.addHouse(house);

        assertThat(streetPlan.getAllHouses(), equalTo(Lists.newArrayList(house)));
    }



    @Test
    public void shouldReturnNumberOfHousesInStreetPlan(){
        assertThat(streetPlan.getNumberOfHouses(), is(0L));

        House house = new House(1, NORTH);
        streetPlan.addHouse(house);

        assertThat(streetPlan.getNumberOfHouses(), is(1L));
    }

    @Test
    public void shouldReturnNumberOfHousesInNorth(){
        assertThat(streetPlan.getNumberOfHousesNorth(), is(0L));

        House house = new House(1, NORTH);
        streetPlan.addHouse(house);

        assertThat(streetPlan.getNumberOfHousesNorth(), is(1L));
    }

    @Test
    public void shouldReturnNumberOfHousesInSouth(){
        assertThat(streetPlan.getNumberOfHousesSouth(), is(0L));

        House house = new House(2, SOUTH);
        streetPlan.addHouse(house);

        assertThat(streetPlan.getNumberOfHousesSouth(), is(1L));
    }
}