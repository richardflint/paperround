package com.valtech.planning.generator;

import com.valtech.street.House;
import com.valtech.street.StreetPlan;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.valtech.street.Side.NORTH;
import static com.valtech.street.Side.SOUTH;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;

public class StreetPlanGeneratorImplTest {
    private StreetPlanGenerator generator;

    @Before
    public void setup(){
        this.generator = new StreetPlanGeneratorImpl();
    }

    @Test
    public void shouldReturnStreetPlanWithOddNumberedHousesInTheNorth(){
        List<String> houseList = Arrays.asList("1", "2", "3");
        StreetPlan streetPlan = generator.generate(houseList);

        assertThat(streetPlan.getHousesInNorth(), hasItems(new House(1, NORTH), new House(3, NORTH)));
    }

    @Test
    public void shouldReturnStreetPlanWithEvenNumberedHousesInTheSouth(){
        List<String> houseList = Arrays.asList("1", "2", "3");
        StreetPlan streetPlan = generator.generate(houseList);

        assertThat(streetPlan.getHousesInSouth(), hasItems(new House(2, SOUTH)));
    }

    @Test(expected = InvalidStreetFile.class)
    public void shouldThrowExceptionWhenDuplicateHouseNumberFound(){
        List<String> houseList = Arrays.asList("1", "1");
        generator.generate(houseList);
    }

    @Test(expected = InvalidStreetFile.class)
    public void shouldThrowExceptionWhenHouseNumberSkipped(){
        List<String> houseList = Arrays.asList("1", "2", "3", "6");
        generator.generate(houseList);
    }

    @Test(expected = InvalidStreetFile.class)
    public void shouldThrowExceptionWhenNoHouseNumberOne(){
        List<String> houseList = Arrays.asList("2", "1");
        generator.generate(houseList);
    }
}