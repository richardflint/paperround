package com.valtech.planning;

import com.valtech.street.StreetPlan;
import com.valtech.planning.generator.InvalidStreetFile;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.io.File;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

@ContextConfiguration("classpath:Cucumber.xml")
public class StreetPlanningDefSteps {

    @Autowired
    private StreetPlanner planner;

    private File streetFile;
    private StreetPlan streetPlan;

    @Given("^A street file \"([^\"]*)\"$")
    public void a_street_file(String streetFilePath) throws Throwable {
        this.streetFile = getResource(streetFilePath);
    }

    @Then("^street plan should be returned with (\\d+) house on the street$")
    public void street_plan_should_be_returned_with_house_on_the_street(int numberOfHouses) throws Throwable {
        assertThat(this.streetPlan.getNumberOfHouses(), is(numberOfHouses));
    }

    @Then("^(\\d+) houses on the north side$")
    public void houses_on_the_north_side(int numberOfHouses) throws Throwable {
        assertThat(this.streetPlan.getNumberOfHousesNorth(), is(numberOfHouses));
    }

    @Then("^(\\d+) houses on the south side$")
    public void houses_on_the_south_side(int numberOfHouses) throws Throwable {
        assertThat(this.streetPlan.getNumberOfHousesSouth(), is(numberOfHouses));
    }

    @Then("^Parsing the file should fail$")
    public void parsing_the_file_should_fail() throws Throwable {
        try {
            this.streetPlan = this.planner.read(streetFile);
            fail("expected exception");
        } catch (Exception e){
            assertThat(e, instanceOf(InvalidStreetFile.class));
        }
    }

    @When("^Parsing the file should return a street plan$")
    public void parsing_the_file_should_return_a_street_plan() throws Throwable {
        this.streetPlan = this.planner.read(streetFile);
    }

    @Then("^total number of houses in the street should be (\\d+)$")
    public void total_number_of_houses_in_the_street_should_be(long numberOfHouses) throws Throwable {
        assertThat(this.streetPlan.getNumberOfHouses(), is(numberOfHouses));
    }

    @Then("^total number of houses on the left hand \\(north\\) of the street should be (\\d+)$")
    public void total_number_of_houses_on_the_left_hand_north_of_the_street_should_be(long numberOfHouses) throws Throwable {
        assertThat(this.streetPlan.getNumberOfHousesNorth(), is(numberOfHouses));
    }

    @Then("^total number of houses on the right hand \\(south\\) side of the street should be (\\d+)$")
    public void total_number_of_houses_on_the_right_hand_south_side_of_the_street_should_be(long numberOfHouses) throws Throwable {
        assertThat(this.streetPlan.getNumberOfHousesSouth(), is(numberOfHouses));
    }

    @SneakyThrows
    private File getResource(String file) {
        return new File(getClass().getResource(file).getFile());
    }

}
