package com.valtech.paperround;

import com.valtech.street.House;
import com.valtech.street.Side;
import com.valtech.street.StreetPlan;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class PaperRoundPlannerDefSteps {

    @Autowired
    private PaperRoundPlanner planner1;

    @Autowired
    private PaperRoundPlanner planner2;

    private StreetPlan streetPlan;
    private PaperRound paperRound;

    @Given("^A street$")
    public void a_street(Map<Integer, Side> street) throws Throwable {
        streetPlan = generateStreetPlan(street);
    }

    @When("^I calculate the route using approach (\\d+)$")
    public void i_calculate_the_route_using_approach(int approach) throws Throwable {
        if(approach == 1) {
            paperRound = planner1.calculateRoute(streetPlan);
        } else {
            paperRound = planner2.calculateRoute(streetPlan);
        }
    }

    @Then("^the order should be$")
    public void the_order_should_be(List<Integer> routeOrder) throws Throwable {
        assertThat(paperRound.getOrderedHouseNumbers(), equalTo(routeOrder));
    }

    @Then("^the number of crosses made should be (\\d+)$")
    public void the_number_of_crosses_made_should_be(long crossings) throws Throwable {
        assertThat(paperRound.getNumberOfRoadCrossings(), is(crossings));
    }

    public StreetPlan generateStreetPlan(Map<Integer, Side> street){
        StreetPlan streetPlan = new StreetPlan();
        street.forEach((houseNumber, side) -> streetPlan.addHouse(new House(houseNumber, side)));
        return streetPlan;
    }
}
