package com.valtech.paperround.routing;

import com.google.common.collect.Lists;
import com.valtech.street.House;
import com.valtech.street.Side;
import com.valtech.street.StreetPlan;
import com.valtech.paperround.PaperRound;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class ApproachTwoStrategyTest {
    private PaperRoundStrategy strategy;

    @Before
    public void setUp(){
        strategy = new ApproachTwoStrategy();
    }

    @Test
    public void shouldReturnHousesReturnHousesInOrderFromWestToEast(){
        List<House> houseList = Lists.newArrayList(new House(1, Side.NORTH), new House(2, Side.SOUTH), new House(3, Side.NORTH), new House(4, Side.SOUTH));

        StreetPlan streetPlan = new StreetPlan();
        houseList.forEach(streetPlan::addHouse);

        PaperRound paperRound = strategy.getPaperRound(streetPlan);

        assertThat(paperRound.getOrderedHouseNumbers(), equalTo(Lists.newArrayList(1, 2, 3, 4)));
    }
}