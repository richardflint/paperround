package com.valtech.paperround;

import com.google.common.collect.Lists;
import com.valtech.street.House;
import org.junit.Test;

import java.util.List;

import static com.valtech.street.Side.NORTH;
import static com.valtech.street.Side.SOUTH;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class PaperRoundTest {

    @Test
    public void shouldReturnListOfHouseNumbers(){
        List<House> houses = Lists.newArrayList(new House(1, NORTH), new House(2, SOUTH));
        PaperRound paperRound = new PaperRound(houses, NORTH);

        assertThat(paperRound.getOrderedHouseNumbers(), equalTo(Lists.newArrayList(1, 2)));
    }

    @Test
    public void shouldReturnNumberOfRoadCrossings(){
        List<House> houses = Lists.newArrayList(new House(1, NORTH), new House(2, SOUTH));
        PaperRound paperRound = new PaperRound(houses, NORTH);

        assertThat(paperRound.getNumberOfRoadCrossings(), is(1L));
    }

    @Test
    public void shouldReturnZeroCrossingWhenAllHousesNorth(){
        List<House> houses = Lists.newArrayList(new House(1, NORTH), new House(3, NORTH));
        PaperRound paperRound = new PaperRound(houses, NORTH);

        assertThat(paperRound.getNumberOfRoadCrossings(), is(0L));
    }
}