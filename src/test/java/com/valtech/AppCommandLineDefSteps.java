package com.valtech;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lombok.SneakyThrows;
import org.springframework.test.context.ContextConfiguration;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

@ContextConfiguration("classpath:Cucumber.xml")
public class AppCommandLineDefSteps {
    private String[] args;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @Given("^Command line args \"([^\"]*)\" \"([^\"]*)\" and \"([^\"]*)\" \"([^\"]*)\"$")
    public void command_line_args_and(String fileOption, String streetFilePath, String modeOption, String mode) throws Throwable {
        File streetFile = getResource(streetFilePath);
        this.args = new String[]{fileOption, streetFile.getAbsolutePath(), modeOption, mode};
    }

    @When("^I run the program$")
    public void i_run_the_program() throws Throwable {
            App.main(this.args);
    }

    @Then("^Screen output should contains \"([^\"]*)\"$")
    public void screen_output_should_contains(String output) throws Throwable {
        assertThat(outContent.toString(), containsString(output));
    }


    @SneakyThrows
    private File getResource(String file) {
        return new File(getClass().getResource(file).getFile());
    }


}