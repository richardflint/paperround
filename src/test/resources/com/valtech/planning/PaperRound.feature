Feature: Paper round route planning
  Scenario: As a newspaper boy or girl given a street, I want to know for approach 1
    Given A street
      | 1 | NORTH |
      | 2 | SOUTH |
      | 3 | NORTH |
      | 4 | SOUTH |
    When I calculate the route using approach 1
    Then the order should be
      | 1 |
      | 3 |
      | 4 |
      | 2 |
    And the number of crosses made should be 1

  Scenario: As a newspaper boy or girl given a street, I want to know for approach 2
    Given A street
      | 1 | NORTH |
      | 2 | SOUTH |
      | 3 | NORTH |
      | 4 | SOUTH |
    When I calculate the route using approach 2
    Then the order should be
      | 1 |
      | 2 |
      | 3 |
      | 4 |
    And the number of crosses made should be 3