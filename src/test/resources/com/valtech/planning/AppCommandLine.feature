Feature: Command Line usage
  Scenario: As a town planner, I want to produce a report for the street specification file
    Given Command line args "-f" "/street_files/street1.txt" and "-m" "planning"
    When I run the program
    Then Screen output should contains "Houses there are in street: 14"
    And Screen output should contains "Houses left hand (north) side of the street: 8"
    And Screen output should contains "Houses right hand (south) side of the street: 6"


  Scenario: As a boy or girl, I want to produce a report of the two approaches for the street specification file
    Given Command line args "-f" "/street_files/street1.txt" and "-m" "paperround"
    When I run the program
    Then Screen output should contains "Route order: [1, 3, 5, 7, 9, 11, 13, 15, 12, 10, 8, 6, 4, 2]"
    And Screen output should contains "Number of crossing: 1"
    And Screen output should contains "Route order: [1, 2, 4, 3, 6, 5, 7, 8, 9, 10, 12, 11, 13, 15]"
    And Screen output should contains "Number of crossing: 8"