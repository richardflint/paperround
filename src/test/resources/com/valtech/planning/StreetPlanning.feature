Feature: Street Planning
  Scenario: Parse invalid file, specification file doesn't contain house number 1
    Given A street file "/street_files/no_house_1.txt"
    Then Parsing the file should fail

  Scenario: Parse invalid file, specification file contains skipped numbers
    Given A street file "/street_files/skipped_house_numbers.txt"
    Then Parsing the file should fail

  Scenario: Parse invalid file, specification file contains house number used more than once
    Given A street file "/street_files/house_number_used_twice.txt"
    Then Parsing the file should fail

  Scenario: As a town planner, I want to know how many houses in given street
    Given A street file "/street_files/street1.txt"
    When Parsing the file should return a street plan
    Then total number of houses in the street should be 14
    And total number of houses on the left hand (north) of the street should be 8
    And total number of houses on the right hand (south) side of the street should be 6