package com.valtech.paperround;

import com.valtech.street.House;
import com.valtech.street.Side;

import java.util.List;
import java.util.stream.Collectors;

public class PaperRound {
    private final List<House> houseRoute;
    private final Side startingSide;

    public PaperRound(List<House> houseRoute, Side startingSide) {
        this.houseRoute = houseRoute;
        this.startingSide = startingSide;
    }

    public List<Integer> getOrderedHouseNumbers() {
        return houseRoute.stream()
                .map(house -> house.getHouseNumber())
                .collect(Collectors.toList());
    }

    public long getNumberOfRoadCrossings() {
        Side lastSideVisited = startingSide;
        long roadCrossings = 0;

        for(House house: houseRoute){
            if(lastSideVisited != house.getSide()){
                roadCrossings++;
                lastSideVisited = house.getSide();
            }
        }

        return roadCrossings;
    }
}
