package com.valtech.paperround.routing;

import com.valtech.street.StreetPlan;
import com.valtech.paperround.PaperRound;

public interface PaperRoundStrategy {
    PaperRound getPaperRound(StreetPlan streetPlan);
}
