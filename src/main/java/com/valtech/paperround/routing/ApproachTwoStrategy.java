package com.valtech.paperround.routing;

import com.valtech.street.House;
import com.valtech.street.StreetPlan;
import com.valtech.paperround.PaperRound;

import java.util.ArrayList;
import java.util.List;

import static com.valtech.street.Side.NORTH;

public class ApproachTwoStrategy implements PaperRoundStrategy {
    @Override
    public PaperRound getPaperRound(StreetPlan streetPlan) {
        List<House> houses = new ArrayList<>();
        streetPlan.getAllHouses().forEach(house -> houses.add(house));

        return new PaperRound(houses, NORTH);
    }
}
