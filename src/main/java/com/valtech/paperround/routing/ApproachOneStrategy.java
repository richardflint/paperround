package com.valtech.paperround.routing;

import com.valtech.street.House;
import com.valtech.street.StreetPlan;
import com.valtech.paperround.PaperRound;

import java.util.ArrayList;
import java.util.List;

import static com.valtech.street.Side.NORTH;

public class ApproachOneStrategy implements PaperRoundStrategy {
    @Override
    public PaperRound getPaperRound(StreetPlan streetPlan) {
        List<House> houses = new ArrayList<>();
        streetPlan.getHousesInNorth().forEach(house -> houses.add(house));

        for(int i = (int) streetPlan.getNumberOfHousesSouth()-1; i >= 0; i--){
            houses.add(streetPlan.getHousesInSouth().get(i));
        }

        return new PaperRound(houses, NORTH);
    }
}
