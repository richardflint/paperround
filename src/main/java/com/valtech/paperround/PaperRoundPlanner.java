package com.valtech.paperround;

import com.valtech.street.StreetPlan;
import com.valtech.paperround.routing.PaperRoundStrategy;

public class PaperRoundPlanner {
    private final PaperRoundStrategy paperRoundStrategy;

    public PaperRoundPlanner(PaperRoundStrategy paperRoundStrategy){
        this.paperRoundStrategy = paperRoundStrategy;
    }

    public PaperRound calculateRoute(StreetPlan streetPlan) {
        return paperRoundStrategy.getPaperRound(streetPlan);
    }
}
