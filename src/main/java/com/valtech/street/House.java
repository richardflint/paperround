package com.valtech.street;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode
@Getter
public class House {
    private final int houseNumber;
    private final Side side;

    public House(int houseNumber, Side side) {
        this.houseNumber = houseNumber;
        this.side = side;
    }
}
