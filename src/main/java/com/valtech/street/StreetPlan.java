package com.valtech.street;

import com.google.common.collect.ImmutableList;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

import static com.valtech.street.Side.NORTH;
import static com.valtech.street.Side.SOUTH;

public class StreetPlan {

    private LinkedHashSet<House> houses = new LinkedHashSet<>();

    public long getNumberOfHouses() {
        return houses.size();
    }

    public long getNumberOfHousesNorth() {
        return houses.stream().filter(house -> house.getSide() == NORTH).count();
    }

    public long getNumberOfHousesSouth() {
        return houses.stream().filter(house -> house.getSide() == SOUTH).count();
    }

    public List<House> getHousesInNorth() {
        return houses.stream().filter(house -> house.getSide() == NORTH).collect(Collectors.toList());
    }

    public List<House> getHousesInSouth() {
        return houses.stream().filter(house -> house.getSide() == SOUTH).collect(Collectors.toList());
    }

    public void addHouse(House house) {
        houses.add(house);
    }

    public List<House> getAllHouses(){
        return ImmutableList.copyOf(houses);
    }
}
