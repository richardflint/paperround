package com.valtech.planning;

import com.valtech.planning.generator.StreetPlanGenerator;
import com.valtech.planning.parser.FileParser;
import com.valtech.street.StreetPlan;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class StreetPlanner {
    private final FileParser fileParser;
    private final StreetPlanGenerator generator;

    @Autowired
    public StreetPlanner(FileParser fileParser, StreetPlanGenerator generator) {
        this.fileParser = fileParser;
        this.generator = generator;
    }

    public StreetPlan read(File streetFile) throws IOException {
        List<String> houseNumbers = fileParser.parse(streetFile);
        return generator.generate(houseNumbers);
    }
}
