package com.valtech.planning.generator;

public class InvalidStreetFile extends RuntimeException{
    public InvalidStreetFile(String message) {
        super(message);
    }
}
