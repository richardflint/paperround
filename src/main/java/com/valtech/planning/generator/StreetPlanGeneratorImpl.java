package com.valtech.planning.generator;

import com.valtech.street.House;
import com.valtech.street.Side;
import com.valtech.street.StreetPlan;

import java.util.List;

import static com.valtech.street.Side.NORTH;
import static com.valtech.street.Side.SOUTH;

public class StreetPlanGeneratorImpl implements StreetPlanGenerator {
    @Override
    public StreetPlan generate(List<String> houseList) {
        if(!isStartingHouseOne(houseList)) throw new InvalidStreetFile("Numbering does not start at 1");

        int housesInNorth = 0;
        int housesInSouth = 0;

        StreetPlan streetPlan = new StreetPlan();

        for(String houseNumber: houseList){
            int number = Integer.parseInt(houseNumber);

            if(getSideOfStreet(number) == NORTH){
                if(number != (housesInNorth*2)+1) throw new InvalidStreetFile("Unable to add House, unexpected house number for north side");
                housesInNorth++;
            } else {
                if(number != (housesInSouth*2)+2) throw new InvalidStreetFile("Unable to add House, unexpected house number for south side");
                housesInSouth++;
            }

            streetPlan.addHouse(new House(number, getSideOfStreet(number)));
        }

        return streetPlan;
    }

    private boolean isStartingHouseOne(List<String> houseList){
        return !houseList.isEmpty() && Integer.parseInt(houseList.get(0)) == 1;
    }

    private Side getSideOfStreet(int number){
        return number % 2 == 0 ? SOUTH : NORTH;
    }
}
