package com.valtech.planning.generator;

import com.valtech.street.StreetPlan;

import java.util.List;

public interface StreetPlanGenerator {
    StreetPlan generate(List<String> houseList);
}
