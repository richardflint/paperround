package com.valtech.planning.parser;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface FileParser {
    List<String> parse(File streetFile) throws IOException;
}
