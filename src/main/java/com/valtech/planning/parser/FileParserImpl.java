package com.valtech.planning.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FileParserImpl implements FileParser{
    @Override
    public List<String> parse(File streetFile) throws IOException {
        try (FileReader reader = new FileReader(streetFile)){
            BufferedReader bufferedReader = new BufferedReader(reader);
            return bufferedReader
                    .lines()
                    .map(line -> line.split(" "))
                    .flatMap(Arrays::stream)
                    .collect(Collectors.toList());
        }
    }
}
