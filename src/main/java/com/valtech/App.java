package com.valtech;

import com.valtech.paperround.PaperRound;
import com.valtech.paperround.PaperRoundPlanner;
import com.valtech.planning.StreetPlanner;
import com.valtech.street.StreetPlan;
import org.apache.commons.cli.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.io.IOException;

public class App {
    public static void main(String[] args) throws IOException {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath*:applicationContext.xml");
        App app = (App) context.getBean("app");
        app.run(args);
    }

    @Autowired
    private StreetPlanner streetPlanner;
    @Autowired
    private PaperRoundPlanner planner1;
    @Autowired
    private PaperRoundPlanner planner2;

    public void run(String[] args) throws IOException {
        CommandLineParser parser = new DefaultParser();
        Options options = new Options();
        options.addOption(OptionBuilder.withLongOpt("file")
                .withDescription("Path to street specification file. e.g. -f \"/tmp/street.txt\"")
                .hasArg()
                .withArgName("FILE")
                .isRequired()
                .create("f"));

        options.addOption(
                OptionBuilder.withLongOpt("mode")
                        .withDescription("-m \"planning\" or -m \"paperround\"")
                        .hasArg()
                        .withArgName("MODE")
                        .isRequired()
                        .create("m"));

        try {
            CommandLine line = parser.parse(options, args);
            String filePath = line.getOptionValue("file");
            StreetPlan streetPlan = streetPlanner.read(new File(filePath));

            if (line.getOptionValue("mode").equals("planning")) {
                planningReport(streetPlan);
            } else if(line.getOptionValue("mode").equals("paperround")){
                paperRoundReport(streetPlan);
            } else {
                throw new ParseException("Invalid arguments");
            }
        } catch (ParseException exp) {
            String header = "Provide a street specification file\n\n";
            String footer = "\n";
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -jar *.jar", header, options, footer, true);
        }
    }

    private void planningReport(StreetPlan streetPlan) {
        System.out.println("******************* Planning Report *******************\n");
        System.out.println("Houses there are in street: " + streetPlan.getNumberOfHouses());
        System.out.println("Houses left hand (north) side of the street: " + streetPlan.getNumberOfHousesNorth());
        System.out.println("Houses right hand (south) side of the street: " + streetPlan.getNumberOfHousesSouth());
        System.out.println("*********************************************************\n");
    }

    private void paperRoundReport(StreetPlan streetPlan) {
        PaperRound paperRound1 = planner1.calculateRoute(streetPlan);
        System.out.println("******************* Paper Round Report *******************\n");
        System.out.println("Paper round approach 1.");
        System.out.println("Route order: " + paperRound1.getOrderedHouseNumbers());
        System.out.println("Number of crossing: " + paperRound1.getNumberOfRoadCrossings());
        PaperRound paperRound2 = planner2.calculateRoute(streetPlan);
        System.out.println("\n");
        System.out.println("Paper round approach 2.");
        System.out.println("Route order: " + paperRound2.getOrderedHouseNumbers());
        System.out.println("Number of crossing: " + paperRound2.getNumberOfRoadCrossings());
        System.out.println("**********************************************************\n");
    }

}